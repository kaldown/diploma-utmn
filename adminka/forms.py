from django import forms
from django.contrib.auth.models import User

from .models import SpecializationTableRow, PatientProfile, DoctorProfile


class DoctorTableForm(forms.Form):
    table_name = forms.CharField(label='Table name', max_length=250)

class DoctorTableFieldsForm(forms.Form):
    field_name = forms.CharField(label='Field name', max_length=100)
    required = forms.BooleanField(label='Required')

class SpecializationTableRowForm(forms.ModelForm):
    row_description = forms.CharField(label='Description', widget=forms.Textarea)
    class Meta:
        model = SpecializationTableRow
        fields = ['row_name', 'row_description', 'row_required']

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password')

class PatientProfileForm(forms.ModelForm):
    class Meta:
        model = PatientProfile
        fields = ('middle_name', 'age', 'policy', 'snils', 'passport_series',
                    'passport_id', 'passport_from', 'passport_date')

class DoctorProfileForm(forms.ModelForm):
    class Meta:
        model = DoctorProfile
        fields = ('middle_name', 'academic_degree')

