from __future__ import unicode_literals

from django.apps import AppConfig


class AdminkaConfig(AppConfig):
    name = 'adminka'
