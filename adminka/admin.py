from django.contrib import admin
from adminka.models import Specialization, Consultation,\
                           SpecializationTable, SpecializationTableRow,\
                           PatientProfile, DoctorProfile, City

from .forms import SpecializationTableRowForm

class SpecializationTableRowAdmin(admin.TabularInline):
    form = SpecializationTableRowForm
    model = SpecializationTableRow
    extra = 1

class SpecializationTableAdmin(admin.ModelAdmin):
    inlines = [ SpecializationTableRowAdmin ]
    #readonly_fields = ( 'table_name', )

admin.site.register(PatientProfile)
admin.site.register(DoctorProfile)
admin.site.register(Consultation)
admin.site.register(Specialization)
admin.site.register(SpecializationTable, SpecializationTableAdmin)
