#-*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User

from django.db import models

# Create your models here.
@python_2_unicode_compatible
class ServiceSpecialization(models.Model):
    QUALIFICATIONS = (
        ('Q1', 'Qualification 1'),
        ('Q2', 'Qualification 2'),
        ('Q3', 'Qualification 3'),
    )
    name = models.TextField()
    doctor = models.ForeignKey('DoctorProfile')
    specialization = models.ForeignKey('Specialization', null=True)
    qualification = models.CharField(choices=QUALIFICATIONS, max_length=3)
    
    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Specialization(models.Model):
    name = models.TextField()
    specialization_table = models.ForeignKey('SpecializationTable', null=True)
    problem = models.TextField(blank=True)
    
    def __str__(self):
        return self.name

class Consultation(models.Model):
    STATUS = (
        ('not', 'Не выбрана'),
        ('pro', 'В процессе'),
        ('que', 'В очереди'),
    )

    client_id = models.ForeignKey('PatientProfile')
    doctor_id = models.ForeignKey('DoctorProfile')
    date_posted = models.DateField(null=True, blank=True)
    date_answered = models.DateField(null=True, blank=True)
    status = models.CharField(choices=STATUS, blank=False, 
                                max_length=3, default='not')

@python_2_unicode_compatible
class SpecializationTable(models.Model):
    name = models.TextField()

    def __str__(self):
        return self.name

class SpecializationTableRow(models.Model):
    row_name = models.TextField()
    row_description = models.TextField()
    row_required = models.BooleanField()
    table_name = models.ForeignKey(SpecializationTable)

@python_2_unicode_compatible
class PatientProfile(models.Model):
    user = models.OneToOneField(User)

    middle_name = models.TextField(blank=True)
    age = models.PositiveIntegerField(null=True, blank=True)
    policy = models.TextField(blank=True)
    snils = models.TextField(blank=True)
    passport_series = models.TextField(blank=True)
    passport_id = models.TextField(blank=True)
    passport_from = models.TextField(blank=True)
    passport_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.middle_name

@python_2_unicode_compatible
class DoctorProfile(models.Model):
    ACADEMIC_DEGREE = (
        ('Phd', 'Philosophy Doctor'),
        ('Bch', 'Bechalor'),
        ('Mst', 'Master'),
    )

    STATUS = (
        ('not', 'Не выбрано'),
        ('don', 'Выполнено'),
        ('pro', 'В процессе'),
        ('que', 'В очереди'),
    )

    user = models.OneToOneField(User)

    middle_name = models.TextField(blank=True)
    academic_degree = models.CharField(choices=ACADEMIC_DEGREE, max_length=3)
    specialization = models.ForeignKey('Specialization', null=True)
    hospital = models.ForeignKey('Hospital', null=True)
    status = models.CharField(choices=STATUS, blank=False, 
                                max_length=3, default='not')

    def __str__(self):
        return self.user.username

@python_2_unicode_compatible
class City(models.Model):
    name = models.TextField(blank=False)
    region = models.TextField(blank=False, null=True)

    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Hospital(models.Model):
    name = models.TextField(blank=False)
    city = models.ForeignKey(City, blank=False, null=True)

    def __str__(self):
        return self.name

