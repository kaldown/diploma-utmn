# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-20 05:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('adminka', '0005_auto_20160512_2354'),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Hospital',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='consultation',
            name='date_answered',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='consultation',
            name='date_posted',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='consultation',
            name='status',
            field=models.CharField(choices=[('not', '\u041d\u0435 \u0432\u044b\u0431\u0440\u0430\u043d\u0430'), ('pro', '\u0412 \u043f\u0440\u043e\u0446\u0435\u0441\u0441\u0435'), ('que', '\u0412 \u043e\u0447\u0435\u0440\u0435\u0434\u0438')], default='not', max_length=3),
        ),
        migrations.AddField(
            model_name='doctorprofile',
            name='specialization',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='adminka.Specialization'),
        ),
        migrations.AddField(
            model_name='doctorprofile',
            name='status',
            field=models.CharField(choices=[('not', '\u041d\u0435 \u0432\u044b\u0431\u0440\u0430\u043d\u043e'), ('don', '\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u043e'), ('pro', '\u0412 \u043f\u0440\u043e\u0446\u0435\u0441\u0441\u0435'), ('que', '\u0412 \u043e\u0447\u0435\u0440\u0435\u0434\u0438')], default='not', max_length=3),
        ),
        migrations.AddField(
            model_name='doctorprofile',
            name='hospital',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='adminka.Hospital'),
        ),
    ]
