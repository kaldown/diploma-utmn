from django.shortcuts import render 
from django.http import HttpResponseRedirect, HttpResponse

from .forms import DoctorTableForm, UserForm, PatientProfileForm,\
                    DoctorProfileForm
#from .models import 

# Create your views here.
def create_table(request):
    if request.method == 'POST':
        form = DoctorTableForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('/thanks/')
    else:
        form = DoctorTableForm()
    return render(request, 'adminka/create_table.html', {'form': form})

def register(request):
    registered = False

    user_class = request.path.split('/adminka/register/')[1]
    if request.method == 'GET' and user_class == 'patient/':
        user_form = UserForm()
        profile_form = PatientProfileForm()
    elif request.method == 'GET' and user_class == 'doctor/':
        user_form = UserForm()
        profile_form = DoctorProfileForm()
    elif request.method == 'POST' and user_class == 'patient/':
        user_form = UserForm(data=request.POST)
        profile_form = PatientProfileForm(data=request.POST)
    elif request.method == 'POST' and user_class == 'doctor/':
        user_form = UserForm(data=request.POST)
        profile_form = DoctorProfileForm(data=request.POST)
    else:
        return render(request, 'adminka/register.html', {'register': register})

    if user_form.is_valid() and profile_form.is_valid():
        user = user_form.save()
        profile = profile_form.save(commit=False)
        profile.user = user
        profile.save()

        registered = True

    return render(request, 'adminka/register.html', {'user_form': user_form,
                                                'profile_form': profile_form,
                                                'registered': registered})


def profile(request):
    editable = False
    user = request.user
    if request.method == 'GET' and user.is_authenticated():
        profile_form = DoctorProfileForm()
        for k, v in profile_form.fields.iteritems():
            v.widget.attrs['readonly'] = True
            
    return render(request, 'adminka/profile.html', {
                                                'profile_form': profile_form})
        
